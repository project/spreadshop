<?php

namespace Drupal\spreadshop\Routing;

use Symfony\Component\Routing\Route;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines dynamic routes.
 *
 * Class SpreadshopRoutes.
 *
 * @category Routing
 * @package Drupal\spreadshop\Routing
 * @license GNU General Public License version 2 or later; see LICENSE.txt
 * @link https://yoursite.com/yourshopname
 */
class SpreadshopRoutes extends ControllerBase {

  /**
   * Private values.
   *
   * @var string
   *   config settings
   */
  private $form;
  private $formValue;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * ManageContext constructor.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(FormBuilderInterface $form_builder) {
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder')
    );
  }

  /**
   * Define dynamic routes.
   *
   * {@inheritdoc}
   *
   * @return array
   *   Return dynamic route for the frontend page
   */
  public function routes(): array {
    $routes = [];
    // Returns an array of Route objects.
    $routes['spreadshop.js_config'] = new Route(
        // Path to attach this route to:
        $this->shopUrl(),
        // Route defaults:
        [
          '_controller' => '\Drupal\spreadshop\Controller\SpreadshopController::spreadshopPage',
        ],
        // Route requirements:
        [
          '_permission'  => 'access content',
        ]
    );
    return $routes;
  }

  /**
   * This function returns the custom shop path.
   *
   * @return string
   *   Return the shop url from settings
   */
  public function shopUrl(): string {
    $this->form = $this->formBuilder->getForm('Drupal\spreadshop\Form\SpreadshopConfigForm');
    $this->formValue = '/' . $this->form['shopUrl']['#value'];

    return $this->formValue;
  }

}
