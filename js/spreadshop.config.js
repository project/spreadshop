/**
 * Contains spreadshop js initialisation.
 *
 * @file
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.jsSpreadshop = {
    attach: function (context, settings) {

      $('head').once('jsSpreadshop').append(
        '<!--suppress ALL --><script> ' +
        'var spread_shop_config = { ' +
        '\n shopName: drupalSettings.js_config.shopName, ' +
        '\n startToken: drupalSettings.js_config.startToken, ' +
        '\n prefix: drupalSettings.js_config.prefix, ' +
        '\n baseId: drupalSettings.js_config.baseId, ' +
        '\n locale: drupalSettings.js_config.locale, ' +
        '\n updateMetadata: drupalSettings.js_config.updateMetadata, ' +
        '\n swipeMenu: drupalSettings.js_config.swipeMenu, ' +
        '\n integrationProvider: drupalSettings.js_config.integrationProvider ' +
        '\n }; <\/script>' +
        '<script src=' + drupalSettings.js_config.prefix + '/shopfiles/shopclient/shopclient.nocache.js><\/script>'
      );
    }
  };
})(jQuery, Drupal, drupalSettings);
