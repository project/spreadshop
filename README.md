# SpreadShop Plugin
#### Author: Stefan Drehmann <drehmann@ironshark.de>
#### Contributors: Beatrice Thom

##### Tags: 
* spreadshirt, shop, spreadshop, shirt shop, t-shirt, 
* spreadshirt plugin, spreadshirt shop, online shop
* shop online, drupal integration, e-commerce, merchandising

INTRODUCTION
------------

With this plugin, you can easily host a top-notch ecommerce shop in Drupal while staying up-to-date with the latest features of your Spreadshop.
This is achieved by embedding your existing Spreadshop as-is into any Drupal page.
Both the plugin and your Spreadshop are fully free of charge. Always.

REQUIREMENTS
------------

* Requires at least: 8.0
* Tested up to: 8.x
* PHP 7.2 minimum
* Stable tag: 1.0.2
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html

INSTALLATION
------------

1. Download the plugin‘s *.zip file.
2. Upload and unzip the *.zip file to your modules/custom folder.
3. Go to the plugin menu (extend) within your drupal admin tool bar.
4. Search for spreadshop.
5. Activate the plugin (hit the checkmark and install).
6. Configure the plugin in the menu available from the top bar.

### What you need

* Installed Drupal instance
* A Spreadshop (which can be registered [here](https://www.spreadshop.com))

CONFIGURATION
------------

### Frequently Asked Questions
* What is the Plugin for?
    * This plugin is for people who are running a Spreadshop (which might look like [this](https://spreadshop.myspreadshop.com/)) and want to make that Spreadshop available in their Drupal system.
    If you do not have a Spreadshop yet, you can [open one free of charge](https://www.spreadshop.com).

* Is there a difference between a stand-alone Spreadshop and the plugin?
    * No, this plugin will simply *embed* your Spreadshop *as-is* into your Drupal system.

* How does this work from a technical point of view?
    * This plugin simply performs a "Website Integration with JavaScript" under the hood, as explained [here](https://help.spreadshop.com/hc/en-us/articles/360010529039-Website-Integration-with-JavaScript).
    The advantage of using this plugin is that you do not need to write any code yourself.

* Can I use the plugin for making my Shop the start page?
    * Most Drupal themes come with a pre-installed startpage layout. 
    The plugin is not able to overwrite this setting. 
    If you want to run the Shop under your own domain and as the start page, 
    we recommend integrating it via 
    Java Script, without a Drupal instance. 
    Read more: 
    https://help.spreadshirt.com/hc/en-us/articles/207487815-Website-Integration-with-JavaScript

* Where can I get my ShopID?
    * When configuring your Spreadshop settings in the "partner area", the shopId is displayed on the bottom left.

* Which platform am I using?
    * Simply put, if you signed up on .com, .ca or .com.au, your Spreadshop runs on the North American platform. All other domains imply you are based on the European platform.
  
* Which Shop URL should I enter?
    * Please enter the page title/path where you’d like your Shop embedded.
    * In our example, the page is called "shop". 
    This is where people will get to see your Shop:
    https://www.yourbrandpage.com/shop
    * Copy the full URL and paste it into your Shop settings under: 
    Advanced Settings > Embed Shop in Website

* How to use the Start Token Setting?
    * You can define a specific page of your Shop 
    that will be loaded in your Drupal page 
    (this could be the detail page/topic overview or a list page).
    For example, using "collections" as a start token will render the topic page.

* What is the MetaData setting about?
    * If you have the Spreadshop plugin running on your website, 
    the relevant integrated SEO meta data changes in your 
    site’s header (title, description and SEO-Index, 
    as well as OpenGraph and Twitter card tags). 
    By unchecking the box, you'll switch this feature off, 
    and your site`s SEO meta data will then be used.

* How to use the Language setting?
    * If your Spreadshop has the "All Languages active" setting enabled, 
    you can define a specific language and 
    currency for your integrated Shop. If you define nothing, 
    the shop's default language and currency will be used.

* What is the Swipe Menu setting?
    * If your Drupal page uses a burger menu on mobile devices,
    we advise you to enable this feature. By doing this, 
    the shop's mobile menu will turn into a swipeable navigation bar 
    (as opposed to a second burger menu).

* Where can I get detailed support?
    * There´s probably someone out there that had exactly the same question 
    and is filled up with wisdom to answer 
    all your requests. Visit our forum and get to know other Shop 
    Owners who can help. 
    There´s one for each platform:
    * EU: https://www.spreadshirt.net/forum
    * NA: https://www.spreadshirt.com/forum

* What If I deactivate the plugin?
    * Spreadshop will disappear from your Drupal system. This, however, does *not* affect your stand-alone Spreadshop in any way.

* How do I uninstall the plugin?
    1. Go to your Drupal plugin (extend) section.
    2. Select the uninstall tab.
    3. Mark the Spreadshop plugin and choose uninstall.
    4. Then hit apply.


# Upgrade Notice

## Changelog

#### 1.0.2
* Migrated to the new myspreadshop domain
* Improved help texts

#### 1.0.1
* Some drupal related fixes and cleanup.

#### 1.0.0
* Initial release for the plugin.
