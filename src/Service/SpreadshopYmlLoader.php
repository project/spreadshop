<?php

namespace Drupal\spreadshop\Service;

use Symfony\Component\Yaml\Yaml;

/**
 * Class SpreadshopYmlLoader.
 *
 * @category Service
 * @package Drupal\spreadshop\Service
 * @license GNU General Public License version 2 or later; see LICENSE.txt
 * @link https://yoursite.com/yourshopname
 */
class SpreadshopYmlLoader extends Yaml {


  /**
   * The namespace for the extension.
   */
  const SPREADSHOP_NAMESPACE = 'spreadshop';

  /**
   * Custom yaml configuration file loader.
   *
   * @param string $fileName
   *   The yml file name.
   * @param string $arrayKey
   *   The array key.
   *
   * @return mixed
   *   Return the yml file with the current path settings
   */
  public function loadYmlFile(string $fileName, string $arrayKey) {

    $ymlFile = Yaml::parse(file_get_contents(dirname(__DIR__, 2) . '/' . self::SPREADSHOP_NAMESPACE . '.' . $fileName . '.yml'));

    return $ymlFile[$arrayKey];
  }

}
