<?php

namespace Drupal\spreadshop\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\spreadshop\Service\SpreadshopYmlLoader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Output of our admin page.
 *
 * @category Form
 * @package Spreadshop
 * @license GNU General Public License version 2 or later; see LICENSE.txt
 * @link https://yoursite.com/yourshopname
 */
class SpreadshopConfigForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * Config file for form settings.
   *
   * @var string
   *   config settings
   */
  const SETTINGS = 'spreadshop.settings';

  /**
   * Value for current url request.
   *
   * @var string
   *   RequestStack value
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request_stack) {
    parent::__construct($config_factory);
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * Form id name.
   *
   * {@inheritdoc}
   *
   * @return string
   *   define a unique form name
   */
  public function getFormId() {
    return 'spreadshop_form';
  }

  /**
   * Configure form fields for backend.
   *
   * {@inheritdoc}
   *
   * @param array $form
   *   Contains all form values.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   return the form elements
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $formSettings = new SpreadshopYmlLoader();
    $formPlatform = $formSettings->loadYmlFile('settings.custom', 'platform');

    $config = $this->config(static::SETTINGS);

    $form['essentialSettings'] = [
      '#type' => 'label',
      '#title' => $this->t('Essential Settings (required)'),
    ];

    $form['shopId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ShopID'),
      '#required' => TRUE,
      '#default_value' => $config->get('shopId'),
    ];

    $form['platform'] = [
      '#type' => 'select',
      '#title' => $this->t('Platform'),
      '#options' => $formPlatform,
      '#default_value' => $config->get('platform'),
      '#ajax' => [
        'callback' => [$this, 'changeOptionsAjax'],
        'wrapper' => 'region_wrapper',
      ],
    ];

    $form['shopUrl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drupal Page Name'),
      '#required' => TRUE,
      '#default_value' => $config->get('shopUrl'),
    ];

    $form['shopUrlHint'] = [
      '#type' => 'label',
      '#title' => $this->t('URL path of your Drupal site where the shop is supposed to show. Please clear the drupal cache after changing this.'),
    ];

    $form['additionalSettings'] = [
      '#type' => 'label',
      '#title' => $this->t('Additional Settings'),
    ];

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Start Token'),
      '#required' => FALSE,
      '#default_value' => $config->get('token'),
    ];

    $form['region'] = [
      '#type' => 'select',
      '#title' => $this->t('Region'),
      '#options' => $this->getOptions($form_state, $config->get('platform')),
      '#prefix' => '<div id="region_wrapper">',
      '#suffix' => '</div>',
      '#default_value' => $config->get('region'),
    ];

    $form['meta'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Meta Data'),
      '#default_value' => $config->get('meta'),
    ];

    $form['swipe'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mobile Swipe Menu'),
      '#default_value' => $config->get('swipe'),
    ];

    $formLink = $formSettings->loadYmlFile('settings.custom', 'link');
    $formLinkIndex = 0;

    foreach ($formLink as $formLinkEntry) {

      $formLinkIndex++;

      (empty($formLinkEntry['url'])) ? $formLinkEntry['url'] = $config->get('shopUrl') : $formLinkEntry['url'];

      $form[$formLinkIndex] = $this->getFormUrl(
        $formLinkEntry['label'],
        $formLinkEntry['url'],
        $formLinkEntry['target'],
        $formLinkEntry['class']
      )->toRenderable();
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Changes'),
    ];

    return $form;
  }

  /**
   * Function to validate form input.
   *
   * {@inheritdoc}
   *
   * @param array $form
   *   Contains all form values.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!preg_match(
        '/^[a-zA-Z0-9\-]+$/', $form_state
          ->getValue('shopId')
    )
    ) {
      $form_state
        ->setErrorByName(
            'shopId', $this
              ->t('The ShopID is not valid. Only use numbers, letters and no spaces.')
        );
    }

    if (!preg_match(
        '/^[a-z\-]+$/', $form_state
          ->getValue('shopUrl')
    )
    ) {
      $form_state
        ->setErrorByName(
            'shopUrl', $this
              ->t('The Shop URL is not valid. Only use lowercase letters and no spaces.')
        );
    }

    if (preg_match(
        '/ +/', $form_state
          ->getValue('token')
    ) && !(empty($form_state->getValue('token')))
    ) {
      $form_state
        ->setErrorByName(
            'token', $this
              ->t('The Start Token is not valid. Only use a valid url token and no spaces.')
        );
    }
  }

  /**
   * Submit form function.
   *
   * {@inheritdoc}
   *
   * @param array $form
   *   Contains all form values.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('shopId', $form_state->getValue('shopId'))
      ->set('platform', $form_state->getValue('platform'))
      ->set('shopUrl', $form_state->getValue('shopUrl'))
      ->set('token', $form_state->getValue('token'))
      ->set('meta', $form_state->getValue('meta'))
      ->set('swipe', $form_state->getValue('swipe'))
      ->set('region', $form_state->getValue('region'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Function to return settings.
   *
   * {@inheritdoc}
   *
   * @return array
   *   Return the const settings name
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Ajax callback to change options for region field.
   *
   * @param array $form
   *   Contains all form values.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   Return current region
   */
  public function changeOptionsAjax(array &$form, FormStateInterface $form_state): array {
    return $form['region'];
  }

  /**
   * Get options for region field.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Contains all form values.
   * @param string $default
   *   Set default value.
   *
   * @return array
   *   Return available options
   */
  public function getOptions(FormStateInterface $form_state, $default): array {
    $formSettings = new SpreadshopYmlLoader();
    $formRegion = $formSettings->loadYmlFile('settings.custom', 'region');

    if ($form_state->getValue('platform')) :
      $region = ($form_state->getValue('platform') == 'eu') ? $formRegion['eu'] : $formRegion['na'];
        elseif (!empty($formRegion[$default])) :
          $region = $formRegion[$default];
        else :
          $region = $formRegion['eu'];
        endif;

        return $region;
  }

  /**
   * Get url for configuration form.
   *
   * @param string $formLabel
   *   The url label.
   * @param string $formUrl
   *   The url destination.
   * @param string $formTarget
   *   The url target.
   * @param string $formClass
   *   The url css class.
   *
   * @return mixed
   *   Return a custom url
   */
  public function getFormUrl($formLabel, $formUrl, $formTarget, $formClass) {

    $url = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    $setUrl = (!filter_var($formUrl, FILTER_VALIDATE_URL)) ? $url . '/' . $formUrl . '#!/' : $formUrl;

    $link = Link::fromTextAndUrl(
        $formLabel, Url::fromUri(
            $setUrl, [
              'attributes' => [
                'target' => $formTarget,
                'class' => $formClass,
              ],
            ]
        )
    );

    return $link;
  }

}
