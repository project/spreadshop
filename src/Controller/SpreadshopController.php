<?php

namespace Drupal\spreadshop\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\spreadshop\Service\SpreadshopYmlLoader;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Output of our spreadshop page.
 *
 * Class SpreadshopController.
 *
 * @category Controller
 * @package Drupal\spreadshop\Controller
 * @license GNU General Public License version 2 or later; see LICENSE.txt
 * @link https://yoursite.com/yourshopname
 */
class SpreadshopController extends ControllerBase {

  /**
   * Private content variables.
   *
   * @var string
   */
  private $form;
  private $formValue;
  private $jsSettings;
  private $integrationProvider;
  private $prefixRegion;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * ManageContext constructor.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(FormBuilderInterface $form_builder) {
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder')
    );
  }

  /**
   * Rendering spreadshop page.
   *
   * @return array
   *   Return the configuration array for the shop
   */
  public function spreadshopPage(): array {
    $build = [
      'content' => [
        '#markup' => "<div id=" . $this->spreadshopFormValue('baseId') . " class='js-sprdnet'></div>",
      ],
      '#attached' => [
        'library' => [
          '0' => 'spreadshop/spreadshop-js-config',
          '1' => 'spreadshop/spreadshop-css-admin',
        ],
        'drupalSettings' => [
          'js_config' => [
            'shopName' => $this->spreadshopFormValue('shopId'),
            'startToken' => $this->spreadshopFormValue('startToken'),
            'prefix' => $this->spreadshopFormValue('prefix'),
            'baseId' => $this->spreadshopFormValue('baseId'),
            'locale' => $this->spreadshopFormValue('locale'),
            'updateMetadata' => (bool)$this->spreadshopFormValue('updateMetadata'),
            'swipeMenu' => (bool)$this->spreadshopFormValue('swipeMenu'),
            'integrationProvider' => $this->spreadshopFormValue('integrationProvider'),
          ],
        ],
      ],
    ];

    return $build;
  }

  /**
   * Prepare form values for spreadshop page.
   *
   * @param string $value
   *   Get the form value.
   *
   * @return string
   *   Return the form values from the form
   */
  public function spreadshopFormValue(string $value): string {

    $this->form = $this->formBuilder->getForm('Drupal\spreadshop\Form\SpreadshopConfigForm');

    $this->jsSettings = new SpreadshopYmlLoader();
    $this->integrationProvider = $this->jsSettings->loadYmlFile('settings.custom', 'version');

    switch ($value) {
      case "shopName":
        $this->formValue = $this->form['shopUrl']['#value'];
        break;

      case "startToken":
        $this->formValue = $this->form['token']['#value'];
        break;

      case "prefix":
        $this->prefixRegion = ($this->form['platform']['#value'] == 'eu') ? 'net' : 'com';
        $this->formValue = "https://" . $this->form['shopId']['#value'] . ".myspreadshop." . $this->prefixRegion;
        break;

      case "baseId":
        $this->formValue = 'spreadshop-placeholder-'.$this->form['shopId']['#value'];
        break;

      case "shopId":
        $this->formValue = $this->form['shopId']['#value'];
        break;

      case "locale":
        $this->formValue = $this->form['region']['#value'];
        break;

      case "updateMetadata":
        $this->formValue = $this->form['meta']['#value'];
        break;

      case "swipeMenu":
        $this->formValue = $this->form['swipe']['#value'];
        break;

      case "integrationProvider":
        $this->formValue = $this->integrationProvider;
        break;

      default:
        $this->formValue = "";
    }

    return $this->formValue;
  }

}
